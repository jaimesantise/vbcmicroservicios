/* Configuracion */
const configuracionServidor = require('./config').get(process.env.NODE_ENV).server;
/* Controladores */
const obtenerServicios = require('../controllers/obtenerServicios');
const crearServicios = require('../controllers/crearServicios');
const eliminarServicio = require('../controllers/eliminarServicio');
const actualizarServicios = require('../controllers/actualizarServicios');

/* Operaciones */
/*istanbul ignore next*/
module.exports = function (app) {
    //OSVT
    app.get('/microservicioVBC/obtenerServicios', obtenerServicios.getServices);
    app.get('/microservicioVBC/obtenerCategorias', obtenerServicios.getCategorias);
    app.post('/microservicioVBC/crearServicios', crearServicios.createServices);
    app.post('/microservicioVBC/eliminarServicio', eliminarServicio.deleteService);
    app.post('/microservicioVBC/actualizarServicio', actualizarServicios.updateServices);

};