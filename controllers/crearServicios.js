"use strict"

const
    db = require('../model/insert'),
    config = require('../config/config').get(process.env.NODE_ENV),
    nombreOperacion = "actualizarServicios";

exports.createServices = function (req, res) {
    try {
        let values = JSON.parse(JSON.stringify(req.body));
        
        db.insert('servicios', values)
        .then(resultado => {
            console.log("INSERT INTO SERVICE - OK");
            res.status(200).send(resultado);
        })
        .catch((error) => {
            console.log("Error en la llamada a la base de datos -> Services:");
            console.log(error);
            return res.status(500).send(error);
        }); 
       

    } catch (error) {
        console.log("Error en metodo updateServices:");
        console.log(error);
        return res.status(500).send(error);
    }
}