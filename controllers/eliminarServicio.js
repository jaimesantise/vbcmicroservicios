"use strict"

const
    db = require('../model/delete'),
    config = require('../config/config').get(process.env.NODE_ENV),
    nombreOperacion = "eliminarServicio";

exports.deleteService = function (req, res) {
    try {
        let values = JSON.parse(JSON.stringify(req.body));
        
        db.deleteDB('servicios', values)
        .then(resultado => {
            console.log("DELETE SERVICE - OK");
            res.status(200).send(resultado);
        })
        .catch((error) => {
            console.log("Error en la llamada a la base de datos -> Services:");
            console.log(error);
            return res.status(500).send(error);
        }); 
       

    } catch (error) {
        console.log("Error en metodo deleteService:");
        console.log(error);
        return res.status(500).send(error);
    }
}