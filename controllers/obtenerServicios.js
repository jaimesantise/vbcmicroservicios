"use strict"

const db = require('../model/select'),
    config = require('../config/config').get(process.env.NODE_ENV),
    nombreOperacion = "obtenerServicios";

exports.getServices = function (req, res) {
    try {
       
        db.select('*', 'servicios')
        .then(resultado => {
            console.log(resultado)
            
            res.status(200).send(resultado)
        })
        .catch((error) => {
            console.log("Error en la llamada a la base de datos -> Services:");
            console.log(error);
            return res.status(500).send(error);
        }); 
       

    } catch (error) {
        console.log("Error en metodo getServices:");
        console.log(error);
        return res.status(500).send(error);
    }
}

exports.getCategorias = function (req, res) {
    try {
       
        db.selectGroupBy('categoria', 'servicios')
        .then(resultado => {
            console.log(resultado)
            
            res.status(200).send(resultado)
        })
        .catch((error) => {
            console.log("Error en la llamada a la base de datos -> Categorias:");
            console.log(error);
            return res.status(500).send(error);
        }); 
       

    } catch (error) {
        console.log("Error en metodo getCategorias:");
        console.log(error);
        return res.status(500).send(error);
    }
}