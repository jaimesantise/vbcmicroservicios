const chai = require('chai'),
    expect = chai.expect,
    should = chai.should(),
    db = require('../model/insert'),
    crearServicio = require("../controllers/insertupdateServicios"),
    response = require('mock-express-response'),
    proxyquire = require('proxyquire');

describe('CrearServiciosController', function(){
    it('Should add an element to the end of a non-full queue', function(){
        const req = {
            headers: {},
            body: {}
        };

        const resultado = {
                mensajeError: "SUCCESS",
                ok: true,
                datos: {}
            
        };

        moduleController = proxyquire('../controllers/insertupdateServicios', {
            "../model/insert":{
                insert(tabla, valores){
                    return new Promise((resolve, reject) => {
                        resolve(resultado);
                    });
                }
            }
        });
        crearServicio.updateServices(req, new response());
    });
});