"use strict";
const mysql = require('mysql2/promise');
const configuracionServidor = require('../config/config').get(process.env.NODE_ENV).server;

async function insert(tabla, values) {
    
    const connection = await mysql.createConnection({
        host: configuracionServidor.host,
        user: configuracionServidor.user,
        password: configuracionServidor.password,
        database: configuracionServidor.database,
        port: configuracionServidor.port,
        ssl: configuracionServidor.ssl
    }); 

    const sentenciaInsert = 'INSERT INTO ' + tabla + ' (nombre, descripcion, duracion, valor, categoria)' 
    + ' VALUES (' 
    + "'" + values["nombre"] + "'," 
    + "'" + values["descripcion"] + "'," 
    + values["duracion"] + "," 
    + values["valor"] + "," 
    + "'" + values["categoria"] + "'" 
    + ');';

    console.log("sentencia :" + sentenciaInsert);

    const [rows, fields] = await connection.execute(sentenciaInsert);

    console.log(rows);
    return JSON.parse(JSON.stringify(rows));

}

module.exports = Object.assign({}, { insert });