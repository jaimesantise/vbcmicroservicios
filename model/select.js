"use strict";
const mysql = require('mysql2/promise');
const configuracionServidor = require('../config/config').get(process.env.NODE_ENV).server;
/*
async function selectWithConditions(columnas, tabla, columnaCondicion, operador, condicion) {

    const connection = await mysql.createConnection({
        host: configuracionServidor.host,
        user: configuracionServidor.user,
        password: configuracionServidor.password,
        database: configuracionServidor.database,
        port: configuracionServidor.port,
        ssl: configuracionServidor.ssl
    }); 

    const [rows, fields] = await connection.execute('SELECT ' + columnas + ' FROM ' + tabla + " WHERE " + columnaCondicion + " " + operador + " " + condicion)
    
    return JSON.parse(JSON.stringify(rows));
}
*/
async function select(columnas, tabla) {
    const connection = await mysql.createConnection({
        host: configuracionServidor.host,
        user: configuracionServidor.user,
        password: configuracionServidor.password,
        database: configuracionServidor.database,
        port: configuracionServidor.port,
        ssl: configuracionServidor.ssl
    }); 

    // query database
    const [rows, fields] = await connection.execute('SELECT ' + columnas + ' FROM ' + tabla);
    console.log(rows);
    return JSON.parse(JSON.stringify(rows));
}

async function selectGroupBy(columnas, tabla) {
    const connection = await mysql.createConnection({
        host: configuracionServidor.host,
        user: configuracionServidor.user,
        password: configuracionServidor.password,
        database: configuracionServidor.database,
        port: configuracionServidor.port,
        ssl: configuracionServidor.ssl
    }); 

    // query database
    const sentencia = 'SELECT ' + columnas + ' FROM ' + tabla + ' GROUP BY ' + columnas;
    console.log(sentencia);
    const [rows, fields] = await connection.execute(sentencia);
    console.log(rows);
    return JSON.parse(JSON.stringify(rows));
}

module.exports = Object.assign({}, { select, selectGroupBy });