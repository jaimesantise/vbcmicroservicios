"use strict";
const mysql = require('mysql2/promise');
const configuracionServidor = require('../config/config').get(process.env.NODE_ENV).server;

async function update(tabla, values) {
    
    const connection = await mysql.createConnection({
        host: configuracionServidor.host,
        user: configuracionServidor.user,
        password: configuracionServidor.password,
        database: configuracionServidor.database,
        port: configuracionServidor.port,
        ssl: configuracionServidor.ssl
    }); 

    const sentenciaInsert = 'UPDATE ' + tabla + ' SET ' +
    "nombre='"+ values["nombre"] + "'," + 
    "descripcion='" + values["descripcion"] + "'," +
    "duracion="  + values["duracion"] + "," +
    "valor="  + values["valor"] + "," +
    "categoria='"  + values["categoria"] + 
    "' WHERE servicioid = " + values["servicioid"] + ';';

    console.log("sentencia :" + sentenciaInsert);

    const [rows, fields] = await connection.execute(sentenciaInsert);

    console.log(rows);
    return JSON.parse(JSON.stringify(rows));

}

module.exports = Object.assign({}, { update });