"use strict";
const mysql = require('mysql2/promise');
const configuracionServidor = require('../config/config').get(process.env.NODE_ENV).server;

async function deleteDB(tabla, values) {
    
    const connection = await mysql.createConnection({
        host: configuracionServidor.host,
        user: configuracionServidor.user,
        password: configuracionServidor.password,
        database: configuracionServidor.database,
        port: configuracionServidor.port,
        ssl: configuracionServidor.ssl
    }); 

    const sentenciaInsert = 'DELETE FROM ' + tabla + ' WHERE servicioid = ' + values["servicioid"];

    console.log("sentencia :" + sentenciaInsert);

    const [rows, fields] = await connection.execute(sentenciaInsert);

    console.log(rows);
    return JSON.parse(JSON.stringify(rows));

}

module.exports = Object.assign({}, { deleteDB });